FROM arm64v8/ubuntu:20.04
FROM python:3.8-slim-buster

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3-dev python3-numpy python3-pip python3-setuptools python3-wheel 
RUN apt-get install -y python3 python3-dev python3-setuptools gcc libtinfo-dev zlib1g-dev build-essential cmake libedit-dev libxml2-dev
RUN apt-get install -y libssl-dev
RUN apt-get install -y git wget llvm clang 
RUN apt-get install ffmpeg libsm6 libxext6  -y
RUN apt-get install -y libgl1-mesa-glx

RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
RUN apt-get install gpg -y
RUN echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null
RUN apt-get update && rm /usr/share/keyrings/kitware-archive-keyring.gpg
RUN apt-get install -y kitware-archive-keyring && apt-get install cmake -y

WORKDIR /
RUN git clone --recursive https://github.com/apache/tvm tvm
RUN mkdir tvm/build
RUN cp tvm/cmake/config.cmake tvm/build
WORKDIR /tvm/build
RUN cmake ..
RUN make -j4
ENV PYTHONPATH=/tvm/python:/tvm/topi/python:${PYTHONPATH}

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install flask
RUN python3 -m pip install numpy
RUN python3 -m pip install boto3
RUN python3 -m pip install tflite_runtime
RUN python3 -m pip install grpcio grpcio-tools
RUN python3 -m pip install opencv-python
RUN python3 -m pip install decorator
RUN python3 -m pip install psutil scipy attrs pytest oss2

WORKDIR /

COPY model_data/ model_data/
COPY config config/
COPY src/ src/
WORKDIR /src/grpc/
CMD python gen.py

WORKDIR /

COPY inference.py inference.py

ENTRYPOINT ["python3", "inference.py"]
