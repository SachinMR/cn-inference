"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2
import numpy as np

def normalize_and_reshape(img, model):
    if model == "yolov3" or model == "tiny_yolov3":                                                        
        img = img.astype(np.float32)                       
        img = np.reshape(img, (1,416,416,3))              
    else:
        img = img.astype(np.float32)
        img = np.reshape(img, (1,416,416,3))       
        
    return img
