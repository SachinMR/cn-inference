"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import json
import os
import tvm
from tvm.contrib import graph_runtime
import numpy as np

from src.postprocess import filter_boxes, _nms_boxes, sigmoid, post_process_tiny_predictions, postprocess_tiny, postprocess_yolo
from src.img_helper import  decode_class_names

def load_model(model_path):
    ctx = tvm.cpu()
    loaded_json = open(model_path + "deploy_graph.json").read()
    loaded_lib = tvm.runtime.load_module(model_path + "deploy_lib.so")
    loaded_params = bytearray(open(model_path + "deploy_param.params", "rb").read())
    model = graph_runtime.create(loaded_json, loaded_lib, ctx)
    model.load_params(loaded_params)
    return model

def load_model_config(model, cpu_cores):
    yolov3_config = json.load(open("config/yolov3.json"))
    tiny_yolov3_config = json.load(open("config/tiny_yolov3.json"))

    os.environ["TVM_NUM_THREADS"] = cpu_cores

    if model == "tiny_yolov3":                                                                         
        model_path = tiny_yolov3_config["model_path"]                                                                              
        class_name_path = tiny_yolov3_config["class_name_path"]                                                                       
        strides = tiny_yolov3_config["strides"]                                                                                                   
        anchors = tiny_yolov3_config["anchors"]                                                        
        mask = tiny_yolov3_config["mask"]                                                                                 
        score_threshold = float(tiny_yolov3_config["score_threshold"])                                                                       
        postprocess = postprocess_tiny                                                                                          
    elif model == "yolov3":                                                                         
        model_path = yolov3_config["model_path"]                                              
        class_name_path = yolov3_config["class_name_path"]                                                     
        strides =  yolov3_config["strides"]                                                                                             
        anchors =  yolov3_config["anchors"]                                       
        mask = yolov3_config["mask"]                                                                                             
        score_threshold = float(yolov3_config["score_threshold"])                                                                      
        postprocess = postprocess_yolo                                                              

    else:                                                                                           
        print("Model Not supported...Selecting the yolov3 model")               
        model_path = yolov3_config["model_path"]                                                                     
        class_name_path = yolov3_config["class_name_path"]                                              
        strides =  yolov3_config["strides"]                                                                                   
        anchors =  yolov3_config["anchors"]                                 
        mask = yolov3_config["mask"]                                                                                      
        score_threshold = float(yolov3_config["score_threshold"])                                                             
        postprocess = postprocess_yolo


    max_outputs = 100                                                                               
    iou_threshold = 0.4 
    names = decode_class_names(class_name_path)                                                     
    num_classes = len(names)                                          
    anchors = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), anchors.split())))     
    mask = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), mask.split())))           
    strides = list(map(int, strides.split(',')))


    return postprocess, score_threshold, mask, anchors, model_path, max_outputs, iou_threshold, names , strides, num_classes
